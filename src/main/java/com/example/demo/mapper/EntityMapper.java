package com.example.demo.mapper;

import com.example.demo.model.database.Entity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel="spring")
public interface EntityMapper {
    EntityMapper INSTANCE = Mappers.getMapper(EntityMapper.class);

    Entity controllerEntityToDbEntity(com.example.demo.model.controller.Entity entity);

    List<Entity> controllerEntitiesToDbEntities(List<com.example.demo.model.controller.Entity> entities);
}
