package com.example.demo.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;

@Service
public class RssUtils {
    public File convertLinkToHtml(String link) throws IOException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(RssUtils.class.getResourceAsStream("/static/XSLT.xslt"));
        Transformer transformer = null;
        try {
            transformer = factory.newTransformer(xslt);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }

        File source = new File("source");
        FileUtils.copyURLToFile(new URL(link), source);
        Source text = new StreamSource(source);
        File result = new File("temporary");
        try {
            transformer.transform(text, new StreamResult(result));
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return result;
    }

    public String convertLinkToHtml(List<String> links, String email) throws IOException {
        File result = new File("target/classes/static/"+ email + ".html");
        for (String link: links) {
            File file = convertLinkToHtml(link);
            IOUtils.copy(new FileInputStream(file), new FileOutputStream(result, true));
        }
        return "";
    }
}
