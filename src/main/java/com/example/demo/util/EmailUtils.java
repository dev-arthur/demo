package com.example.demo.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.PreDestroy;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class EmailUtils {

	@Autowired
	private JavaMailSender mailSender;

	@Value("${email.login}")
	private String emailLogin;

	// Send MIME Email
	public void sendMimeEmailMessage(String email, String mailSubject, String mailContent, Boolean isHtml) {

		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
			message.setFrom(new InternetAddress(emailLogin));
			if(StringUtils.isBlank(email)) {
				return;
			}
			message.setTo(email);
			message.setSubject(mailSubject);
			if (isHtml)
				mimeMessage.setContent(mailContent != null ? mailContent : "Empty Content", "text/html; charset=UTF-8");
			else
				mimeMessage.setContent(mailContent != null ? mailContent : "Empty Content", "text; charset=UTF-8");
			mailSender.send(mimeMessage);
			log.info("Mime Message has been sent to %s ", email);
		} catch (MessagingException e) {
			log.info("Message Was Not Sent!");
			throw new MailParseException(e);
		}

	}
}
