package com.example.demo.model.controller;

import lombok.Data;

import java.util.List;

@Data
public class Entity {
    private String email;
    private List<String> links;
}
