package com.example.demo.model.database;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@javax.persistence.Entity
@Table(name = "ENTITY")
public class Entity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false)
    private Long id;
    private String email;
    @ElementCollection
    private List<String> links;
}
