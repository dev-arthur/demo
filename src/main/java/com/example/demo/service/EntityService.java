package com.example.demo.service;

import com.example.demo.mapper.EntityMapper;
import com.example.demo.model.controller.Entity;
import com.example.demo.repository.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntityService {
    @Autowired
    private EntityRepository entityRepository;

    public void save(Entity entity) {
        entityRepository.save(EntityMapper.INSTANCE.controllerEntityToDbEntity(entity));
    }
}
