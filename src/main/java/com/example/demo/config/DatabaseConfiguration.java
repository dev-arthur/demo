package com.example.demo.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.Executor;

@Configuration
@ComponentScan("com.example.demo.model.database")
@EnableJpaRepositories(basePackages = "com.example.demo.repository",
        entityManagerFactoryRef = "mainEntityManagerFactory",
        transactionManagerRef = "mainTransactionManager")
@EnableTransactionManagement
@EnableAsync
@EntityScan("com.example.demo.model.database")
public class DatabaseConfiguration extends HikariConfig implements AsyncConfigurer {

    @Value(value = "${spring.datasource.hikari.jdbc-url}")
    private String url;

    @Value(value = "${spring.datasource.hikari.username}")
    private String username;

    @Value(value = "${spring.datasource.hikari.password}")
    private String password;

    @Value(value = "${spring.datasource.hikari.driver-class-name}")
    private String dataSourceClassName;

    @Value(value = "${spring.jpa.hibernate.ddl-auto}")
    private String ddlAuto;

    @Bean
    @Primary
    public DataSource dataSource() {

        Properties dataSourceProperties = new Properties();
        dataSourceProperties.put("user", username);
        dataSourceProperties.put("password", password);

        Properties configProperties = new Properties();
        configProperties.put("jdbcUrl", url);
        configProperties.put("driverClassName", dataSourceClassName);
        configProperties.put("poolName", "H2ConnectionPool");
        configProperties.put("maximumPoolSize", 30);
        configProperties.put("connectionTimeout", 10000);
        configProperties.put("minimumIdle", 10000);
        configProperties.put("idleTimeout", 10000);
        configProperties.put("dataSourceProperties", dataSourceProperties);

        HikariConfig hikariConfig = new HikariConfig(configProperties);
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(4);
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }

    @Bean
    @Primary
    PlatformTransactionManager mainTransactionManager() {
        return new JpaTransactionManager(mainEntityManagerFactory().getObject());
    }

    @Bean
    @Primary
    LocalContainerEntityManagerFactoryBean mainEntityManagerFactory() {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

        factoryBean.setDataSource(dataSource());
        factoryBean.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", ddlAuto);
        factoryBean.setJpaPropertyMap(properties);
        factoryBean.setPackagesToScan("com.example.demo.model.database");

        return factoryBean;
    }
}
