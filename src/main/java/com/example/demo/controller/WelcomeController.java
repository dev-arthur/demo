package com.example.demo.controller;

import com.example.demo.model.controller.Entity;
import com.example.demo.service.EntityService;
import com.example.demo.util.EmailUtils;
import com.example.demo.util.RssUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.ListUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

@Controller
@SessionAttributes({"entity", "previewUrl"})
public class WelcomeController {

    @Autowired
    private RssUtils rssUtils;

    @Autowired
    private EntityService entityService;

    @Autowired
    private EmailUtils emailUtils;

    @RequestMapping(value = {"", "/", "/welcome"}, method = RequestMethod.GET)
    public String mainPage(Model model) {
        return "welcome";
    }

    @RequestMapping(value = {"/saveEmail"}, method = RequestMethod.POST)
    public String saveEmail(@RequestParam String email, Model model) {
        Entity entity = new Entity();
        entity.setEmail(email);
        model.addAttribute("entity", entity);
        return "welcome";
    }

    @RequestMapping(value = {"/addRssLink"}, method = RequestMethod.POST)
    public String addRssLink(@RequestParam String link, Model model) throws IOException {
        Entity entity = (Entity) model.getAttribute("entity");
        if (entity == null) {
            entity = new Entity();
            model.addAttribute("entity", entity);
        }
        if (ListUtils.isEmpty(entity.getLinks())) {
            entity.setLinks(new ArrayList<>());
        }
        entity.getLinks().add(link);
        model.addAttribute("previewUrl", entity.getEmail() + ".html");
        rssUtils.convertLinkToHtml(entity.getLinks(), entity.getEmail());
        return "welcome";
    }

    @RequestMapping(value = {"/deleteLink"}, method = RequestMethod.POST)
    public String deleteRssLink(@RequestParam String link, Model model) {
        Entity entity = (Entity) model.getAttribute("entity");
        if (ListUtils.isEmpty(entity.getLinks())) {
            entity.setLinks(new ArrayList<>());
        }
        entity.getLinks().remove(link);
        return "welcome";
    }

    @RequestMapping(value = {"/saveToDb"}, method = RequestMethod.POST)
    public String saveToDb(Model model) {
        entityService.save((Entity) model.getAttribute("entity"));
        return "welcome";
    }

    @RequestMapping(value = {"/sendEmail"}, method = RequestMethod.POST)
    public String sendEmail(Model model) throws IOException {
        Entity entity = (Entity) model.getAttribute("entity");
        emailUtils.sendMimeEmailMessage(entity.getEmail(),
                "RESULTS",
                Files.readString(Paths.get("target/classes/static/"+ entity.getEmail() + ".html"), StandardCharsets.UTF_8),
                true);
        return "welcome";
    }


}
