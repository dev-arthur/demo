Project was tested with rss links from here: https://www.feedforall.com/sample-feeds.htm

Before starting the project you will need to set up gmail login and password in application.yml file
To make it work with gmail you will need to:
 - turn off two step verification and turn on less secure apps here https://myaccount.google.com/u/0/security?gar=1
 - or simple create password for applications and use it 
 
To run application please use DemoApplication.class 
After launch go to http://localhost:8080/ and you will see ui. 
First step you will need to setup email address. 
After that you can add rss links and delete them. 

By clicking "SAVE" all data will be saved in db. To check this go to 
http://localhost:8080/console and login with credentials from application.yml (default are sa:sa)
By clicking "SEND" email with html will be sent to your email from the first step.